<?php

/*
 *
 * General Odintra Settings
 *
 */

// When a user successfully logs in where should we send them. This is a RELATIVE path to your Odintra so '/' = home
// Hint: This value is parsed to CodeIgniter's siteurl() function
$config['authenticated_redirect'] = '/';

/*
 *
 * Security Settings
 *
 */

// What should we do if this user is not on the whitelist?
// TRUE = Show a page which informs the user that they are not on the whitelist
// FALSE = Call die() function, do not display anything to the user
$config['not_whitelisted_display'] = true;

// If a user tries to access a page to which they have no permission - do we redirect them to the home page?
// If this is set to true there will be no opportunity to display that they have no access permission
$config['no_access_redirect'] = false;

// If a user tries to access a page to which they have no permission - do we tell them they have no permission?
$config['no_access_display'] = true;

// Number of login attempts allowed - defaults 5, means account will be locked out on the 5th unsuccessful attempt
$config['failed_attempts_allowed'] = 5;

// Number of seconds an account is locked for following the maximum attempts. Defaults to 900 (15 minutes)
$config['failed_attempts_locked_time'] = '900';

// The name of the theme that we are using - defaults to 'Odintra'
$config['theme'] = 'Odintra';