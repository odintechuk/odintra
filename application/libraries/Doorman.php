<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *
 * Doorman
 *
 * The doorman is responsible for identifying whether or not a user has permission to access the content they are trying
 * by utilising and testing Authentication and Permissions and Whitelisting
 *
 */
class Doorman{

    protected $ci;

    protected $permission_required = '';
    protected $login_required = true;

    // When we startup all we need to do is load in CodeIgniter
    public function __construct(){

        $this->ci =& get_instance();

    }

    // Check if the access is allowed here
    public function check_access( $login_required = true, $permission = '' ){

        $this->login_required = $login_required;
        $this->permission_required = $permission;

        // If this user is not on the whitelist we need to deny them any access to the system
        if ( !$this->check_whitelist() ){

            // Based on the configuration we may or may not display an error message
            if ( $this->ci->config->item( 'not_whitelisted_display' ) === true ){
                echo $this->ci->load->view('errors/not_whitelisted', array(), true );
            }

            // As this user is not on the whitelist we do not want them to have any access to the system. Kill execution instantaneously
            die();

        }

        // Now we know they are allowed to access the intranet, check to see if authentication is required
        if ( $this->login_required === true ){

            // Login is required, and this user is not logged in, send them to the login page
            if ( $this->ci->authenticator->is_logged_in() !== true ){
                redirect( site_url('/login') );
            }

        }

        // Now we've handled that bit, let's handle the permissions
        // @TODO in a further commit - enable permissions and handling

        // They've not failed any of the tests, allow them access
        return true;

    }

    // Returns TRUE of they are on the whitelist (or the whitelist is empty), otherwise FALSE
    protected function check_whitelist(){

        // Load the whitelist configuration
        $whitelist = $this->ci->config->item('odintra_whitelist');

        // If the whitelist is empty - assume this is a publicly accessible intranet
        if ( empty( $whitelist ) ){
            return true;
        }

        // If the user's IP is whitelisted, we will allow access pending authentication
        if ( in_array( $this->ci->input->ip_address(), $whitelist, false ) ){
            return true;
        }

        // There is a whitelist, this IP is not on it, return FALSE
        return false;

    }

}