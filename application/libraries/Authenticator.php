<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *
 * Authenticator
 *
 * The authenticator is responsible authenticationof users (login and password verification)
 *
 */
class Authenticator{

    // Load in an instance of CodeIgniter so we can utilise the functionality
    protected $ci;

    // The ID of the user who is currently logged in
    protected $user_id = 0;

    // The session ID that we get from CodeIgniter
    protected $session_id = '';

    // Authenticated is set when we run is_logged_in
    protected $authenticated = null;

    // This allows us to only ever check if the user is logged in once, even if is_logged_in is executed multiple times
    protected $check_run = false;

    // Attempt to load session information
    public function __construct(){

        // Load in CodeIGniter
        $this->ci =& get_instance();

        // Get the Session ID from CodeIgniter
        $this->session_id = $this->ci->session->session_id;

    }

    // Return the user's session ID
    public function get_session_id(){
        return $this->session_id;
    }

    // Returns TRUE or FALSE
    public function is_logged_in(){

        // If we have already checked there's no reason to fire against the database again, return the previous result
        if ( $this->check_run === true ){
            return $this->authenticated;
        }

        // Lookup the session based on this user's session key
        $session_lookup = $this->ci->sessions->lookup( $this->session_id );

        // The session was not found in the database, this user is not authenticated
        if ( empty( $session_lookup ) ){
            $this->authenticated = false;
            return false;
        }

        // Look up the associated user
        $user_lookup = $this->ci->users->lookup( $session_lookup->user_id );

        // The user was not found, this user is not authenticated (this prevents orphaned sessions authenticating)
        if ( empty( $user_lookup ) ){
            $this->authenticated = false;
            return false;
        }

        // We've passed every test, we are authenticated:
        // - Make note that we have run this check
        // - Set the value of authenticated to TRUE
        // - Return TRUE (for when this is the first run)
        $this->check_run = true;
        $this->authenticated = true;
        return true;

    }

    // Attempt to login based on the email address and password provided
    public function attempt_login( $email, $password ){

        // Load in the user's details based on the email address
        $details = $this->ci->users->get_details_by_email( $email );

        // No details returned - this can't be a valid login attempt
        if ( empty( $details ) ){
            return false;
        }

        // Ensure this account is not locked (brute force protection)
        if ( $details->account_locked == 1 && strtotime( $details->account_unlock ) > time() ){
            return false;
        }

        // If the password does not verify this can't be a valid login attempt
        if ( !$this->verify_password( $password, $details->password ) ){
            $this->ci->users->log_invalid_auth_attempt( $details );
            return false;
        }

        // Process the successful login - this redirects the user so no return is necessary, however in case that method
        // is changed we'll return TRUE if we get that far, just in case
        $this->handle_successful_login( $details );
        return true;

    }

    // Verify a password against its hash. This has been abstracted so it is easy to change as time goes on and new security
    // measures are put in place
    public function verify_password( $password, $hash ){
        if( !password_verify( $password, $hash ) ){
            return false;
        }
        return true;
    }

    // This is what we will do when a user is successfully logged in
    protected function handle_successful_login( $user_record ){

        $this->ci->load->helper('url');

        // Create the session log in the database
        $this->ci->sessions->create( $this->session_id, $user_record->id );

        // Reset any failed logins
        $this->ci->users->clear_failed_logins( $user_record );

        // Redirect to the relative URL that is specified in the Odintra configurations
        redirect( site_url( $this->ci->config->item( 'authenticated_redirect' ) ) );

    }

}