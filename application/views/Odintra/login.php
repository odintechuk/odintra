<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login - Odintra</title>

    <!-- jQuery via CDN -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <style>
        .panel-primary{
            margin-top: 20px;
        }
    </style>

</head>
<body>

<div id="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h1 class="text-center">System Login</h1>
                    <p class="text-center">Your all in one intranet solution</p>
                </div>
                <div class="panel-body">
                    <?php if( $display_fail === true ): ?>
                        <p class="alert alert-danger">You could not be logged in using those details</p>
                    <?php elseif( $display_logged_out === true ): ?>
                        <p class="alert alert-success">You have been successfully logged out</p>
                    <?php endif; ?>
                    <p>In order to access your intranet you will need to provide valid login details, login attempts may be recorded and subject to limitation at the discretion of your system administrator</p>

                    <?=
                        form_open('') .
                        form_label('Email Address', 'email_address') .
                        form_input( array( 'name' => 'email_address', 'id' => 'email_address', 'type' => 'email', 'class' => 'form-control' ) ) .
                        '<br />' .
                        form_label('Password', 'password') .
                        form_input( array( 'name' => 'password', 'id' => 'password', 'type' => 'password', 'class' => 'form-control' ) ) .
                        '<br />' .
                        form_submit( array( 'class' => 'btn btn-primary', 'value' => 'Login' ) ) .
                        form_close()
                    ?>
                </div>
                <div class="panel-footer">
                    <p class="text-center"><small>Powered by <a href="http://odintra.net">Odintra</a></small></p>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>