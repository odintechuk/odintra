<?php

	/*
	 *
	 * This page may never be rendered, depending on the configurations
	 * It is the page which is displayed if you try to access the intranet from outside of a trusted IP
	 *
	 */

defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Access Denied - Not On Network</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: darkred;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: darkred;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
		text-align: center;
		text-transform: uppercase;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px auto;
		background: white;
		border: 1px solid #D0D0D0;
		width: 500px;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>You are not whitelisted</h1>

	<div id="body">
		<p>The network or computer that you are trying to access this intranet on is not on the approved list of IP addresses, as such you cannot be granted access to the system.</p>
		<p>Please contact your network administrator to resolve this issue.</p>
	</div>
</div>

</body>
</html>