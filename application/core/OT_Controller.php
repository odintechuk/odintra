<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OT_Controller extends CI_Controller{

    // The default active permission required to access this controller
    protected $active_permission = '';

    // Whether or not authentication is required for this page, realistically it's only login and password reset that might
    // not need this, but it allows for controllers to utilise our extension without requiring authentication
    protected $authentication_required = true;

    /*
     *
     * Construct Method
     * ----------------
     * Called for all controllers that are loaded. Here we implement the security and other autoloading that is required
     * for Odintra to work properly
     *
     */
    public function __construct(){

        // We still want to inherit the CodeIgniter standard Controller __construct functionality
        parent::__construct();

        // Use the doorman to check all security stuffs
        $this->doorman->check_access( $this->authentication_required, $this->get_active_permission() );

    }

    // This method can be extended to facilitate different permissions within controllers
    protected function get_active_permission(){
        return $this->active_permission;
    }

    // Routing around $this->load->view so we're not hacking core CI functionality, but are including themability
    protected function load_view( $name, $data = array(), $return = false ){

        // Does the view exist? If so, load it in via CI and return the response
        if ( file_exists( APPPATH.'views/' . $this->config->item('theme') . '/' . $name . '.php' ) ){
            return $this->load->view( $this->config->item('theme') . '/' . $name, $data, $return );
        }

        // View does not exist in that theme, so we will load in the default
        return $this->load->view('Odintra/' . $name, $data, $return );

    }

}