<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends OT_Controller {

    protected $authentication_required = false;

    public function index()
    {

        // We don't allow logged in users to try and log in again, send them to the home page
        if ( $this->authenticator->is_logged_in() ){
            redirect( site_url() );
        }

        // Load in the helpers we need for this functionality - forms and validation
        $this->load->helper('form');
        $this->load->library('form_validation');

        // Email address and password are both required for submission
        $rules = array(
            array(
                'field' => 'email_address',
                'label' => 'Email Address',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            )
        );

        // Set the validation rules (as above)
        $this->form_validation->set_rules( $rules );

        // Should we display the fail message - no as standard
        $display_fail = false;

        // The form has validated, so we know there is a POST login attempt
        if ( $this->form_validation->run() ){

            // Attempt the login, if this fails then we will need to display and failed message as users are automatically
            // redirected if their attempt is successful
            if ( !$this->authenticator->attempt_login( $this->input->post('email_address'), $this->input->post('password') ) ){
                $display_fail = true;
            }

        }

        // Should we display the logged out successfully message? Odintra - no
        $display_logged_out = false;

        // If specified in the URL (parsed from the logout controller) - then we'll display the logout message
        if ( !empty( $this->input->get('loggedout') ) ){
            $display_logged_out = true;
        }

        // Prepare to parse data to the view
        $details = array(
            'email' => $this->input->post('email_address'),
            'display_fail' => $display_fail,
            'display_logged_out' => $display_logged_out
        );

        // Load the login view
        $this->load_view( 'login', $details );

    }
}
