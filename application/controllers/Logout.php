<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends OT_Controller {

    public function index()
    {

        // Destroy the session in the database
        $this->sessions->destroy( $this->authenticator->get_session_id() );

        // Destroy the user's session
        $this->session->sess_destroy();

        // Redirect the user to the login page, with a message saying they've been logged out
        redirect( site_url( '/login?loggedout=true' ) );

    }
}
