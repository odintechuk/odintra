<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sessions extends CI_Model{

    public function __construct(){
        parent::__construct();
    }

    public function lookup( $session ){
        $query = $this->db->get_where('sessions', array( 'session_key' => $session ) );
        $result = $query->result();
        if ( empty( $result ) ){
            return false;
        } elseif ( count( $result ) > 1 ){
            return false;
        }
        return $result[0];
    }

    public function create( $session, $user_id ){
        $record = new stdClass();
        $record->session_key = $session;
        $record->user_id = $user_id;
        $record->created = date('Y-m-d H:i:s');
        $record->ip_address = $this->input->ip_address();
        $this->db->insert( 'sessions', $record );
    }

    public function destroy( $session ){
        $this->db->delete('sessions', array('session_key' => $session ) );
    }

}