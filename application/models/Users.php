<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Model{

    // Find and return a user by their ID
    public function lookup( $id ){
        $query = $this->db->get_where('users', array( 'id' => $id ) );
        $result = $query->result();

        // Ensure we have found one, and only one, result
        if ( empty( $result ) || count( $result ) > 1 ){
            return false;
        }

        // Results are returned in an array, we're only returning one record, so return it
        return $result[0];

    }

    // Load user details by email address
    public function get_details_by_email( $email_address ){

        // Find an execute the query to find this user
        $query = $this->db->get_where('users', array('email_address' => $email_address));
        $result = $query->result();

        // We we get anythign other than one result this is unreliable, so return false;
        if ( empty( $result ) || count( $result ) > 1 ){
            return false;
        }

        // We found one record - this is reliable, return the result
        return $result[0];

    }

    // Logs an invalid authentication attempt against a user
    public function log_invalid_auth_attempt( $user_record ){

        // Increment the counter
        $user_record->failed_login_attempts++;

        // If we have exceeded the configuration defined amount of attempts which are allowed
        if ( $user_record->failed_login_attempts >= $this->config->item('failed_attempts_allowed') ){

            // Lock the account
            $user_record->account_locked = 1;

            // From configuration figure out when this account will be allowed again
            $user_record->account_unlock = date('Y-m-d H:i:s', ( time() + $this->config->item('failed_attempts_locked_time' ) ) );

        }

        // Update the user record
        $this->db->where( 'id', $user_record->id );
        $this->db->update( 'users', $user_record );

    }

    // User has logged in, clear any failed logins
    public function clear_failed_logins( $user_record ){

        // Set login attempts and account locked to 0, set the unlock time as null
        $user_record->failed_login_attempts = 0;
        $user_record->account_locked = 0;
        $user_record->account_unlock = null;

        // Update the record
        $this->db->where( 'id', $user_record->id );
        $this->db->update( 'users', $user_record );

    }

}