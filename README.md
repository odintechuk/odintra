# Odintra

Odinta is an open source intranet software built on PHP designed for LAMP environments, it is distributed as an open source under the MIT License

# Useful Links

* Odintra Website - http://odintra.net
* News and Updates - http://odintra.net/news
* Odintra Documentation - http://odintra.net/docs
* Technical Support - http://odintra.net/support
* Enterprise - http://odintra.net/enterprise
* Code Repository - https://bitbucket.org/odintechuk/odintra/overview
* Existing Issues - https://bitbucket.org/odintechuk/odintra/issues?status=open
* Developer Documentation (Core Contributors) - https://bitbucket.org/odintechuk/odintra/wiki/

# With Thanks

We are not officially affiliated with, or endorsed by, any of the following projects, but we would like to send them thanks for their open source contributions which have been utilised in Odintra in one way or another. These projects are all free and open source, and we really encourage you to use them, because they're awesome and because without them Odintra would not have happened

* PHP - https://php.net - Using which all of projects rely
* Laravel - https://laravel.com - Which powers the Odintra website
* CodeIgniter - https://codeigniter.com - Which powers the Odintra package
* Bootstrap - https://getbootstrap.com - Which makes responsive web design easy
* Sass - https://sass-lang.com - Which we have utilised on our website and the Project
* FontAwesome - http://fontawesome.io - Icons and spinners
* Google Fonts - https://fonts.google.com

## Contributors

We would also like to thank the contributors to the project

### Core Development

The following people have contributed to the core development of the project

* Matt Johnson of Odin Tech Ltd - http://odintech.uk

### Alpha and Beta Testing

The following people have contributed to Alpha and Beta testing of the Project

* Testers coming soon

### Documentation

The following people have contributed documentation, either core or user documents, for the Project

* Documentation writers coming soon

### Issue Raisers

The following people have contributed issues (and/or solutions) to the Project

* Issue raisers coming soon

# MIT License

Copyright (c) 2016 Odin Tech Ltd. and contributors of the Odintra Project

This project extends CodeIgniter, which is also released under the MIT license. You can find the repository for CodeIgniter and their licensing on their GitHub repository here: https://github.com/bcit-ci/CodeIgniter

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.